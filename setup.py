try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

setup(name='Flaskstrap',
      packages=[
          'flaskstrap',
          'flaskstrap.db',
          'flaskstrap.views',
          ],
      version='0.0.5')
