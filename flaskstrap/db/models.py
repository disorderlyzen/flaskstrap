from flask.ext.login import UserMixin
from sqlalchemy.orm.exc import NoResultFound
from . import db, dbutils
from ..crypto import bcrypt

class ActiveModel(object):
    """A mixin intended to provide convenient functions for repeated SQLAlchemy
    patterns.
    """
    def save(self, commit=True):
        """Adds the current state of the object to the db session and COMMITs
        the transaction if requested.

        Returns a reference to self for convenience
        """
        db.session.add(self)

        if commit:
            dbutils.full_commit()

        return self

    def expire(self):
        db.session.expire(self)

    def refresh(self):
        db.session.refresh(self)

class User(UserMixin, ActiveModel, db.Model):
    """Represents every person in the database.
    """
    id = db.Column(db.Integer, primary_key=True)
    active = db.Column(db.Boolean, nullable=False, default=True)
    verified = db.Column(db.Boolean, nullable=False, default=False)
    name = db.Column(db.String(100), nullable=False)
    email = db.Column(db.String(50), nullable=False, unique=True)
    password = db.Column(db.String(60), nullable=False)

    @classmethod
    def get_by_credentials(cls, email, password):
        """Returns a user that has this email address and validated password.
        """
        try:
            user = cls.query.filter_by(email=email).one()
        except NoResultFound:
            return

        # a user with this email address was found, so if the password
        # hashes correctly, return it
        if bcrypt.check_password_hash(user.password_hash, password):
            return user

    @classmethod
    def create(cls, name, email, password, commit=True):
        user = cls(name=name, email=email,
                password=bcrypt.generate_password_hash(password))

        return user.save(commit)

    def is_active(self):
        """Checks to make sure the user is still active.
        """
        return self.active and self.verified

    def __repr__(self):
        return '<User(id={id}, name={name}, email={email})>'.format(id=self.id,
                name=self.name, email=self.email)

org_user = db.Table('org_user', db.metadata,
        db.Column('org_id', db.Integer, db.ForeignKey('org.id'),
            primary_key=True),
        db.Column('user_id', db.Integer, db.ForeignKey(User.id),
            primary_key=True)
        )

class Org(ActiveModel, db.Model):
    """An organization represents the subdomain or custom domain. It's the
    highest-level grouping of a set of teams and their members.
    """
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(256), nullable=False)
    users = db.relationship(User, secondary=org_user, backref='orgs')

    @classmethod
    def create(cls, app, name):
        org = cls(name=name)
        org.save()
        dbutils.create_organization_schema(app, org.id)
        return org

    def __repr__(self):
        return '<Org({})>'.format(self.name)

class OrgDomain(ActiveModel, db.Model):
    org_id = db.Column(db.Integer, db.ForeignKey(Org.id), primary_key=True)
    subdomain = db.Column(db.String(20), nullable=True, unique=True)
    domain = db.Column(db.Text, nullable=True, unique=True)
