from flask.ext.sqlalchemy import SQLAlchemy

db = SQLAlchemy()

from . import models

__all__ = ['db', 'models']
