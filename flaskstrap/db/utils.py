from contextlib import wraps
from . import db

def get_public_tables(app):
    return _get_tables(app, only_public=True)

def get_org_tables(app):
    return _get_tables(app, only_public=False)

def _get_tables(app, only_public):
    public_tables = app.config.get('PUBLIC_TABLES', [])
    return [table for table in db.get_tables_for_bind()
            if only_public == (table.name in public_tables)]

def full_commit():
    try:
        db.session.commit()
    except:
        db.session.rollback()
        raise

def create_schema(name, commit=True):
    db.session.execute('CREATE SCHEMA {}'.format(name))
    if commit:
        full_commit()

def set_search_path(search_path, commit=True):
    db.session.execute('SET search_path TO {}'.format(search_path))
    if commit:
        full_commit()

def reset_search_path(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        try:
            return fn(*args, **kwargs)
        finally:
            set_search_path('public')

    return wrapper

def create_tables(tables):
    db.metadata.create_all(db.engine, tables=tables)

@reset_search_path
def create_organization_schema(app, id):
    """Creates the schema and tables for the requested organization id.
    """
    create_schema(get_org_schema_name(id))
    use_org_schema(id)
    create_tables(get_org_tables(app))

def get_org_schema_name(id):
    return 'org_{:d}'.format(id)

def use_org_schema(id, include_public=True):
    search_path = [get_org_schema_name(id)]

    if include_public:
        search_path.append('public')

    set_search_path(','.join(search_path))
