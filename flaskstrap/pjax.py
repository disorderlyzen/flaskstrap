from flask import render_template, request

def render_pjax(full_template, pjax_template, **kwargs):
    """Conditionally renders the PJAX template if a PJAX request was made,
    otherwise the full page template.
    """
    if 'X-PJAX' in request.headers:
        template = pjax_template
    else:
        template = full_template
        kwargs['pjax_template'] = pjax_template

    return render_template(template, **kwargs)
