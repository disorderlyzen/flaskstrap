import os

def configure_from_env(package_name):
    """Given an environment, this function will look for all variable names
    that start with the package name where this function resides (usually the
    name of the application itself) and returns a generator of the key/val
    pairs that match.
    """
    package_name = package_name.upper()

    offset = len(package_name) + 1

    env_vars = {env_var_name[offset:]: env_var_value
            for env_var_name, env_var_value in os.environ.iteritems()
            if env_var_name.startswith(package_name)}

    globals().update(env_vars)
