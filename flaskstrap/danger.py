from flask import current_app, url_for
from itsdangerous import URLSafeTimedSerializer
from werkzeug.local import LocalProxy

timed_serializer = LocalProxy(lambda:
        URLSafeTimedSerializer(current_app.config['ITSDANGEROUS_SECRET']))

def get_timed_url(endpoint, payload, salt, token_key='token', **url_args):
    encoded_payload = timed_serializer.dumps(payload, salt)
    url_args[token_key] = encoded_payload
    return url_for(endpoint, **url_args)

def decode_timed_url(token, salt, max_age=None):
    return timed_serializer.loads(token, max_age=max_age,
            return_timestamp=True, salt=salt)
