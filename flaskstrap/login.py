from flask.ext.login import LoginManager
from flask.ext.principal import Principal

login_manager = LoginManager()
principal = Principal()
