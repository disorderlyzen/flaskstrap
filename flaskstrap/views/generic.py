from flask import redirect, render_template, url_for
from flask.ext.login import login_required
from flask.views import MethodView
from ..pjax import render_pjax

class GenericView(MethodView):
    """The base generic view class.
    """
    def dispatch_request(self, *args, **kwargs):
        """Sets self.args and self.kwargs from the request for convenience, in
        case there is a context in the code where the data is not directly
        passed in, but you need access to it.
        """
        self.args = args
        self.kwargs = kwargs
        return super(GenericView, self).dispatch_request(*args, **kwargs)

    @classmethod
    def add_url_rule(cls, app, rule, endpoint):
        """Convenience for registering this view on an app or blueprint,
        responding to the provided rule and given the specified endpoint name.
        """
        app.add_url_rule(rule, view_func=cls.as_view(endpoint))

class TemplateMixin(object):
    """A mixin for rendering a template. Also supports pjax.
    """
    template_name = None
    pjax_template_name = None

    def render_template(self, **context):
        """Renders a template with the provided context. If a pjax template is
        defined, it will attempt to serve it if the request headers indicated
        that a PJAX response is needed.
        """
        if self.pjax_template_name:
            return render_pjax(self.template_name, self.pjax_template_name,
                    **context)

        return render_template(self.template_name, **context)

    def get_context_data(self, **context):
        """Returns a dictionary used as context for the template rendering.
        """
        return context

class TemplateView(TemplateMixin, GenericView):
    """A view that renders a template on a GET request.
    """
    def get(self, *args, **kwargs):
        """Gets the context data and renders a template with it.
        """
        context = self.get_context_data(**kwargs)
        return self.render_template(**context)

class FormMixin(TemplateMixin):
    """A mixin for processing a form.
    """
    form_cls = None
    success_endpoint = None

    def get_form_kwargs(self):
        """Any arguments that need to be passed to the form class constructor
        should be defined here and returned as a dict.
        """
        return {}

    def get_form(self):
        """Instantiates a new form from the form class and any arguments that
        are defined for its constructor through self.get_form_kwargs()
        """
        return self.form_cls(**self.get_form_kwargs())

    def get_success_url(self):
        """Returns a URL that the view will redirect to by default when the
        form is validated.
        """
        return url_for(self.success_endpoint)

    def form_valid(self, form):
        """Default behavior on form validation is to simply redirect to the URL
        returned from self.get_success_url()
        """
        return redirect(self.get_success_url())

    def form_invalid(self, form):
        """If a form is not valid, the default behavior is to render the
        template and including the form as context.
        """
        context = self.get_context_data(form=form)
        return self.render_template(**context)
        
class FormView(FormMixin, GenericView):
    """A view that handles rendering a form on a GET request and processing the
    form on a POST request.
    """
    def get(self, *args, **kwargs):
        """Creates a form and includes it as context to a template that will be
        rendered.
        """
        form = self.get_form()
        context = self.get_context_data(form=form)
        return self.render_template(**context)

    def post(self, *args, **kwargs):
        """Creates a form and validates it. If validation is successful, it
        will call self.form_valid(form). Otherwise, it will call
        self.form_invalid(form). The default behavior for each of these methods
        is documented in the FormMixin class.
        """
        form = self.get_form()

        if form.validate_on_submit():
            return self.form_valid(form)

        return self.form_invalid(form)

    def put(self, *args, **kwargs):
        """If someone actually uses a PUT request, just defer the handling to
        the self.post() method.
        """
        return self.post(*args, **kwargs)

class LoginRequiredMixin(object):
    """A mixin that can be used to verify a user is logged in.
    """
    decorators = [login_required]
