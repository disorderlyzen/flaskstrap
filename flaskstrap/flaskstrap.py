import os
from flask import Flask
from flask_foundation import Foundation
from .babel import babel
from .db import db
from .env import configure_from_env
from .login import login_manager, principal

class Flaskstrap(object):
    def __init__(self, app=None, config_file=None):
        self.app = app or Flask(__name__)
        self.configure(self.app, config_file)

    def configure(self, app, config_file):
        # temporarily use the local directory to get the default config
        prev_root_path = app.config.root_path
        app.config.root_path = os.path.dirname(__file__)
        app.config.from_pyfile('config_default.py')
        app.config.root_path = prev_root_path

        # load the configs from the real application
        if config_file:
            app.config.from_pyfile(config_file, silent=True)

        # local overrides during development
        app.config.from_pyfile('config_local.py', silent=True)

        # look at the environment variables
        env_prefix = app.config.get('ENV_PREFIX')
        if env_prefix:
            configure_from_env(env_prefix)

    def init_db(self):
        # initialize the db
        db.init_app(self.app)

    def init_login_manager(self):
        # initialize the login manager
        login_manager.init_app(self.app)

    def init_permissions(self):
        principal.init_app(self.app)

    def init_localization(self):
        babel.init_app(self.app)

    def init_foundation(self):
        Foundation(self.app)
